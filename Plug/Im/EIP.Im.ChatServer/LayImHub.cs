﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace EIP.Im.ChatServer
{
    /// <summary>
    /// 聊天服务器端:基于SignalR
    /// </summary>
    [HubName("LayImHub")]
    public class LayImHub : Hub
    {

    }
}