//=====================================================================
//
// THIS CODE AND INFORMATION IS PROVIDED TO YOU FOR YOUR REFERENTIAL
// PURPOSES ONLY, AND IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE,
// AND MAY NOT BE REDISTRIBUTED IN ANY MANNER.
//
// Copyright (C) 2003  Microsoft Corporation.  All rights reserved.
//
//=====================================================================

using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Web.UI;
using EIP.Ssom.Service;

namespace EIP.Ssom.Web
{
    /// <summary>
    /// DatabaseProperties.aspx displays the properties of the currently selected database.
    /// The user is able to change file growth quotas for both data and logs.
    /// </summary>
    public partial class DatabaseProperties : Page
    {
        public DatabaseProperties()
        {
            Page.Init += new EventHandler(Page_Init);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SqlServer server = SqlServer.CurrentServer;

            try
            {
                server.Connect();
            }
            catch (Exception ex)
            {
                //Response.Redirect("Error.aspx?errorPassCode=" + 2002);
                Response.Redirect(String.Format("error.aspx?errormsg={0}&stacktrace={1}", Server.UrlEncode(ex.Message), Server.UrlEncode(ex.StackTrace)));
            }

            try
            {
                SqlDatabase database = SqlDatabase.CurrentDatabase(server);

                SqlDatabaseProperties props = database.GetDatabaseProperties();

                NamePropertyLabel.Text = Server.HtmlEncode(props.Name);
                StatusPropertyLabel.Text = Server.HtmlEncode(props.Status);
                OwnerPropertyLabel.Text = Server.HtmlEncode(props.Owner);
                DateCreatedPropertyLabel.Text = Server.HtmlEncode(Convert.ToString(props.DateCreated));
                SizePropertyLabel.Text = props.Size.ToString("f2");
                SpaceAvailablePropertyLabel.Text = props.SpaceAvailable.ToString("f2");
                NumberOfUsersPropertyLabel.Text = Convert.ToString(props.NumberOfUsers);
            }
            catch (COMException ex)
            // Thrown if GetDatabaseProperties fails due to lack of permissions
            {
                //Response.Redirect("Error.aspx?errorPassCode=" + 2001);
                Response.Redirect(String.Format("error.aspx?errormsg={0}&stacktrace={1}", Server.UrlEncode(ex.Message), Server.UrlEncode(ex.StackTrace)));
            }
            catch (Exception ex)             // Catch any unknown errors
            {
                //Response.Redirect("Error.aspx");    // Display user-friendly error page
                Response.Redirect(String.Format("error.aspx?errormsg={0}&stacktrace={1}", Server.UrlEncode(ex.Message), Server.UrlEncode(ex.StackTrace)));
            }
            finally
            {
                server.Disconnect();
            }

            // On first load of the page, force data gathering...
            if (!IsPostBack)
                CancelButton_Click(null, null);
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Page.User.Identity.IsAuthenticated)
            {
                Page.ViewStateUserKey = Page.Session.SessionID;
            }
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
        }

        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion

        protected void CancelButton_Click(object sender, EventArgs e)
        {
            ErrorLabel.Visible = false;

            SqlServer server = SqlServer.CurrentServer;

            try
            {
                server.Connect();
            }
            catch (Exception ex)
            {
                //Response.Redirect("Error.aspx?errorPassCode=" + 2002);
                Response.Redirect(String.Format("error.aspx?errormsg={0}&stacktrace={1}", Server.UrlEncode(ex.Message), Server.UrlEncode(ex.StackTrace)));
            }

            try
            {
                SqlDatabase database = SqlDatabase.CurrentDatabase(server);

                SqlDatabaseProperties props = database.GetDatabaseProperties();

                DataFileProperties.Properties = props.DataFile;
                LogFileProperties.Properties = props.LogFile;
            }
            catch (COMException ex)
            // Thrown if GetDatabaseProperties fails due to lack of permissions
            {
                //Response.Redirect("Error.aspx?errorPassCode=" + 2001);
                Response.Redirect(String.Format("error.aspx?errormsg={0}&stacktrace={1}", Server.UrlEncode(ex.Message), Server.UrlEncode(ex.StackTrace)));
            }
            catch (Exception ex)             // Catch any unknown errors
            {
                //Response.Redirect("Error.aspx");    // Display user-friendly error page
                Response.Redirect(String.Format("error.aspx?errormsg={0}&stacktrace={1}", Server.UrlEncode(ex.Message), Server.UrlEncode(ex.StackTrace)));
            }
            finally
            {
                server.Disconnect();
            }
        }

        protected void ApplyButton_Click(object sender, EventArgs e)
        {
            ErrorLabel.Visible = false;

            SqlServer server = SqlServer.CurrentServer;
            try
            {
                server.Connect();
            }
            catch (Exception ex)
            {
                //Response.Redirect("Error.aspx?errorPassCode=" + 2002);
                Response.Redirect(String.Format("error.aspx?errormsg={0}&stacktrace={1}", Server.UrlEncode(ex.Message), Server.UrlEncode(ex.StackTrace)));
            }

            SqlDatabase database = SqlDatabase.CurrentDatabase(server);

            // Grab data from the form
            SqlDatabaseProperties props = null;

            SqlFileProperties dataFileProperties = null;
            SqlFileProperties logFileProperties = null;

            try
            {
                dataFileProperties = DataFileProperties.Properties;
            }
            catch (Exception ex)
            {
                ErrorLabel.Visible = true;
                ErrorLabel.Text = "错误读取数据文件的属性: " + Server.HtmlEncode(ex.Message).Replace("\n", "<br>") + "<br><br>";
                return;
            }

            try
            {
                logFileProperties = LogFileProperties.Properties;
            }
            catch (Exception ex)
            {
                ErrorLabel.Visible = true;
                ErrorLabel.Text = "错误的阅读日志文件属性: " + Server.HtmlEncode(ex.Message).Replace("\n", "<br>") + "<br><br>";
                return;
            }

            props = new SqlDatabaseProperties(dataFileProperties, logFileProperties);
            SqlDatabaseProperties origProps = database.GetDatabaseProperties();

            // First validate input ourselves
            ArrayList errorList = new ArrayList();

            if (props.DataFile.FileGrowth < 0)
                errorList.Add("数据文件必须为正增长");

            if (props.DataFile.MaximumSize < -1)
                errorList.Add("数据文件的最大大小必须是正的");

            if (props.LogFile.FileGrowth < 0)
                errorList.Add("日志文件必须为正增长");

            if (props.LogFile.MaximumSize < -1)
                errorList.Add("日志文件的最大大小必须是积极的");

            if (props.DataFile.MaximumSize != -1 && origProps.Size > props.DataFile.MaximumSize)
                errorList.Add("最大文件增长必须大于或等于当前数据库的大小");

            if (errorList.Count > 0)
            {
                ErrorLabel.Visible = true;

                ErrorLabel.Text = "以下错误发生:<br><ul>";
                for (int i = 0; i < errorList.Count; i++)
                    ErrorLabel.Text += String.Format("<li>{0}</li>", (string)errorList[i]);
                ErrorLabel.Text += "</ul>";

                return;
            }

            // Try to set properties
            try
            {
                database.SetDatabaseProperties(props);
            }
            catch (Exception ex)
            {
                // Show error message and quit
                server.Disconnect();

                ErrorLabel.Text = "以下错误发生:<br>" + Server.HtmlEncode(ex.Message).Replace("\n", "<br>") + "<br><br>";
                return;
            }

            // Only reload data if there were no errors
            // Get database properties and fill in their info
            props = database.GetDatabaseProperties();

            DataFileProperties.Properties = props.DataFile;
            LogFileProperties.Properties = props.LogFile;

            server.Disconnect();
        }
    }
}