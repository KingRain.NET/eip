<%@ Control Language="c#" Inherits="EIP.Ssom.Web.FileProperties" CodeFile="FileProperties.ascx.cs" %>
<asp:CheckBox Runat="server" Text="文件自动增长" ID="AutomaticallyGrowFileCheckBox"></asp:CheckBox>
<br>
<br>
&nbsp;&nbsp;&nbsp;&nbsp; 文件增长
<br>
&nbsp;&nbsp;&nbsp;&nbsp;
<asp:DropDownList Runat="server" ID="GrowthTypeDropDownList">
    <asp:ListItem Text="以兆字节为单位"></asp:ListItem>
    <asp:ListItem Text="通过百分比"></asp:ListItem>
</asp:DropDownList>
<asp:TextBox Runat="server" Columns="4" ID="GrowthTextBox"></asp:TextBox>
<br>
<br>
&nbsp;&nbsp;&nbsp;&nbsp; 最大文件大小<br>
&nbsp;&nbsp;&nbsp;&nbsp;<asp:RadioButton Runat="server" GroupName="MaximumFileSizeType" Text="不受限制的文件增长" ID="UnrestrictedGrowthRadioButton"></asp:RadioButton><br>
&nbsp;&nbsp;&nbsp;&nbsp;<asp:RadioButton Runat="server" GroupName="MaximumFileSizeType" Text="文件增长限制" ID="RestrictGrowthRadioButton"></asp:RadioButton>
&nbsp;<asp:TextBox Runat="server" Columns="4" ID="MaximumFileSizeTextBox"></asp:TextBox>
MB
<br>

