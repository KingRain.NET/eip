//=====================================================================
//
// THIS CODE AND INFORMATION IS PROVIDED TO YOU FOR YOUR REFERENTIAL
// PURPOSES ONLY, AND IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE,
// AND MAY NOT BE REDISTRIBUTED IN ANY MANNER.
//
// Copyright (C) 2003  Microsoft Corporation.  All rights reserved.
//
//=====================================================================

using System;
using System.Web.UI;
using EIP.Ssom.Service;

namespace EIP.Ssom.Web
{
    /// <summary>
    /// Create a new empty database on the current server.
    /// </summary>
    public partial class CreateDatabase : Page
    {
        public CreateDatabase()
        {
            Page.Init += new EventHandler(Page_Init);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
        }

        protected void CreateNewDatabaseButton_Click(object sender, EventArgs e)
        {
            // If database name is empty or invalid, quit immediately
            if (!IsValid)
                return;

            SqlServer server = SqlServer.CurrentServer;

            // Create the database

            ErrorCreatingLabel.Visible = false;

            bool success = true;

            try
            {
                server.Connect();
            }
            catch (Exception ex)
            {
                //Response.Redirect("Error.aspx?errorPassCode=" + 2002);
                Response.Redirect(String.Format("error.aspx?errormsg={0}&stacktrace={1}", Server.UrlEncode(ex.Message), Server.UrlEncode(ex.StackTrace)));
            }

            // Check that database doesn't exist
            if (server.Databases[DatabaseNameTextBox.Text] != null)
            {
                ErrorCreatingLabel.Visible = true;
                ErrorCreatingLabel.Text = "一个数据库具有该名称已经存在.";
                server.Disconnect();
                return;
            }

            try
            {
                SqlDatabase newDatabase = server.Databases.Add(DatabaseNameTextBox.Text);
            }
            catch (Exception ex)
            {
                ErrorCreatingLabel.Visible = true;
                ErrorCreatingLabel.Text = "创建数据库时发生错误.<br>" + Server.HtmlEncode(ex.Message).Replace("\n", "<br>");
                success = false;
            }

            server.Disconnect();

            if (success)
                Response.Redirect("Tables.aspx?database=" + Server.UrlEncode(DatabaseNameTextBox.Text));
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Page.User.Identity.IsAuthenticated)
            {
                Page.ViewStateUserKey = Page.Session.SessionID;
            }
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
        }

        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
    }
}