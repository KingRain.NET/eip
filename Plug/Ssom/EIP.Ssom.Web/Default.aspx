<%@ Page Language="c#" Inherits="EIP.Ssom.Web.Login" CodeFile="Default.aspx.cs" %>

<%@ Register TagPrefix="Toolbar" TagName="HelpLogout" Src="Toolbars/HelpLogoutToolbar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Web数据管理员 - 登录</title>
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="stylesheet" type="text/css" href="admin.css">
    <style type="text/css">
        .auto-style1 {
            padding: 4px;
            width: 48px;
        }
    </style>
</head>

<script language="javascript" src="Global.js"></script>

<body bottommargin="0" leftmargin="0" topmargin="0" rightmargin="0">
    <form id="WebForm1" method="post" runat="server">
        <%--<table width="100%" cellspacing="0" cellpadding="0" border="0">
            <!-- FIRST ROW: HEADER -->
            <tr>
                <td colspan="3" valign="bottom" align="left" width="100%" height="36" style="background-image:url(images/bg_horizontal_top_right.gif);
                    background-repeat:repeat-x; background-color:#c0c0c0">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <!--BEGIN ONE LINE-->
                            <td valign="bottom" width="308">
                                <img src="images/logo_top.gif" width="308" height="36" alt="" border="0" /></td>
                            <!--END ONE LINE-->
                            <td valign="bottom" align="right" width="100%">
                                <Toolbar:HelpLogout runat="server" ID="HelpLogout" HelpTopic="login"></Toolbar:HelpLogout>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- FIRST ROW: HEADER -->
            <!-- SECOND ROW: CRUMBS -->
          
        </table>--%>
        <!-- SECOND ROW: CRUMBS -->
        <!-- THIRD ROW: BOTTOM SECTION -->
        <table cellspacing="0" cellpadding="0" width="100%" height="90%" border="0">
            <tr>
                <!-- START NAVIGATION SECTION -->
                <%-- <td bgcolor="#6699ff" valign="top" align="center" height="100%" style="width: 160px;"
                    colspan="">
                </td>--%>
                <!-- END NAVIGATION SECTION -->
                <!-- START CONTENT SECTION -->
                <td style="width:100%;" align="center" valign="middle">

                    <table cellspacing="0" cellpadding="0" border="0" width="500px">
                        <tr>
                            <!--BEGIN ONE LINE-->
                            <td valign="bottom" colspan="2" height="8" width="100%">
                                <img src="images/spacer.gif" width="1" height="8" alt="" border="0" />
                            </td>
                            <!--END ONE LINE-->
                        </tr>
                        <tr>
                            <!--BEGIN ONE LINE-->
                            <td align="left" width="12" style="height: 281px">
                                <img src="images/spacer.gif" width="12" height="1" alt="" border="0" /></td>
                            <!--END ONE LINE-->
                            <td align="left" class="databaseListItem" style="width: 400px; height: 281px; padding: 20px; background-color: #b4c6f3; background-image: url(images/bg_login.gif); background-repeat: repeat">
                                <!-- PAGE CONTENT: START -->
                                <asp:Label ID="LogoutInfoLabel" runat="server" Visible="False">你现在登录退出了</asp:Label>
                                <asp:Label ID="LoginInfoLabel" runat="server" Visible="False" Font-Size="10 pt" Font-Bold="true">MSSQL数据库在线管理.</asp:Label>
                                <br />
                                <br />
                                <asp:Label ID="lblCredMsg" runat="server" Font-Size="8 pt">请输入你的用户名和密码:</asp:Label>
                                <br />
                                <br />
                                <table border="0" cellpadding="0" cellspacing="1">
                                    <tr>
                                        <td class="auto-style1">用户名</td>
                                        <td class="databaseListItem">
                                            <asp:TextBox ID="UsernameTextBox" runat="server" Columns="35"></asp:TextBox></td>
                                        <td class="databaseListItem">
                                            <asp:RequiredFieldValidator ID="UsernameRequiredFieldValidator" runat="server" ErrorMessage="必须指定一个用户名"
                                                ControlToValidate="UsernameTextBox" Display="Dynamic"></asp:RequiredFieldValidator></td>
                                    </tr>
                                    <tr>
                                        <td class="auto-style1">密码</td>
                                        <td class="databaseListItem">
                                            <asp:TextBox ID="PasswordTextBox" runat="server" Columns="35" TextMode="Password"></asp:TextBox></td>
                                        <td class="databaseListItem"></td>
                                    </tr>
                                    <tr>
                                        <td class="auto-style1">服务器</td>
                                        <td class="databaseListItem">
                                            <asp:TextBox ID="ServerTextBox" runat="server" Columns="35"></asp:TextBox></td>
                                        <td class="databaseListItem">
                                            <asp:RequiredFieldValidator ID="ServerRequiredFieldValidator" runat="server" ErrorMessage="必须指定一个服务器的名字"
                                                ControlToValidate="ServerTextBox" Display="Dynamic"></asp:RequiredFieldValidator></td>
                                    </tr>
                                    <tr valign="top">
                                        <td class="auto-style1">
                                            <asp:Label ID="lblAuth" runat="server">验证方式</asp:Label></td>
                                        <td class="databaseListItem" nowrap>
                                            <asp:RadioButtonList ID="AuthRadioButtonList" runat="server" AutoPostBack="True"
                                                OnSelectedIndexChanged="AuthRadioButtonList_SelectedIndexChanged">
                                                <asp:ListItem Value="windows" Selected="True">Windows集成</asp:ListItem>
                                                <asp:ListItem Value="sql">SQL登录</asp:ListItem>
                                            </asp:RadioButtonList></td>
                                        <td class="databaseListItem"></td>
                                    </tr>
                                    <tr>
                                        <td class="databaseListItem" colspan="2" align="right">
                                            <asp:Button ID="LoginButton" CssClass="button" onMouseOver="this.style.color='#808080';"
                                                onMouseOut="this.style.color='#000000';" runat="server" Text="登录" OnClick="LoginButton_Click" Height="25px" Width="50px"></asp:Button>
                                        </td>
                                        <td class="databaseListItem"></td>
                                    </tr>
                                </table>
                                <br />
                                <asp:Label ID="ErrorLabel" runat="server" Visible="False" ForeColor="red"></asp:Label>
                                <!-- PAGE CONTENT: END -->
                            </td>
                        </tr>
                    </table>

                </td>
            </tr>
            <!-- THIRD ROW: BOTTOM SECTION -->
        </table>
    </form>
</body>
</html>
