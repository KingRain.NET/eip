//=====================================================================
//
// THIS CODE AND INFORMATION IS PROVIDED TO YOU FOR YOUR REFERENTIAL
// PURPOSES ONLY, AND IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE,
// AND MAY NOT BE REDISTRIBUTED IN ANY MANNER.
//
// Copyright (C) 2003  Microsoft Corporation.  All rights reserved.
//
//=====================================================================

using System;
using EIP.Ssom.Service;

namespace EIP.Ssom.Web
{
    /// <summary>
    /// Summary description for Login.
    /// </summary>
    public partial class Login : System.Web.UI.Page
    {
        Security security = new Security();

        public Login()
        {
            Page.Init += new EventHandler(Page_Init);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (ServerTextBox.Text != null && ServerTextBox.Text.Length == 0)
            {
                ServerTextBox.Text = "."; // "localhost";
            }

            ErrorLabel.Visible = false;
            LogoutInfoLabel.Visible = false;
            LoginInfoLabel.Visible = false;

            if (Request["error"] != null)
            {
                switch (Request["error"])
                {
                    case "sessionexpired":
                        ErrorLabel.Text = "您的会话已经过期或者已经注销。请再次输入您的登录信息,重新连接.";
                        break;
                    case "userinfo":
                        ErrorLabel.Text = "无效的用户名和/或密码,或服务器不存在.";
                        break;
                    default:
                        ErrorLabel.Text = "一个未知的错误发生.";
                        break;
                }
                ErrorLabel.Visible = true;
            }
            else if (Request["action"] == "logout")
            {
                LogoutInfoLabel.Visible = true;
            }
            else
            {
                LoginInfoLabel.Visible = true;
            }

            if (!IsPostBack)
            {
                if (Request.IsAuthenticated == true && security.WebServer == "iis")
                {
                    UsernameTextBox.Text = User.Identity.Name;
                    lblCredMsg.Text = "请输入一个SQL服务器名称:";
                    UsernameTextBox.Enabled = false;
                    PasswordTextBox.Enabled = false;
                }
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Page.User.Identity.IsAuthenticated)
            {
                Page.ViewStateUserKey = Page.Session.SessionID;
            }
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
        }

        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion

        protected void LoginButton_Click(object sender, System.EventArgs e)
        {
            if (!IsValid)
                return;

            bool useIntegrated;
            SqlServer server;
            Security security = new Security();

            if (AuthRadioButtonList.SelectedItem.Value == "windows")
            {
                if (security.WebServer == "iis" && System.Security.Principal.WindowsIdentity.GetCurrent().Name != this.UsernameTextBox.Text)
                {
                    ErrorLabel.Visible = true;
                    ErrorLabel.Text = "IIS verion SQL的Web管理不支持windows登录其他比你自己的.<br>";
                }

                try
                {
                    server = new SqlServer(ServerTextBox.Text, this.UsernameTextBox.Text, this.PasswordTextBox.Text, true);
                    useIntegrated = true;
                }
                catch (System.ComponentModel.Win32Exception w32Ex)
                {
                    ErrorLabel.Visible = true;
                    ErrorLabel.Text = "无效的用户名和/或密码,或服务器不存在.";
                    return;
                }
                catch (Exception ex)
                {
                    ErrorLabel.Visible = true;
                    ErrorLabel.Text = ex.Message;
                    return;
                }
            }
            else
            {
                server = new SqlServer(ServerTextBox.Text, UsernameTextBox.Text, PasswordTextBox.Text, false);
                useIntegrated = false;
            }

            if (server.IsUserValid())
            {
                if (useIntegrated)
                {
                    AdminUser.CurrentUser = new AdminUser(UsernameTextBox.Text, PasswordTextBox.Text, ServerTextBox.Text, true);
                    security.WriteCookieForFormsAuthentication(server.Username, server.Password, false, SqlLoginType.NTUser);
                }
                else
                {
                    AdminUser.CurrentUser = new AdminUser(UsernameTextBox.Text, PasswordTextBox.Text, ServerTextBox.Text, false);
                    security.WriteCookieForFormsAuthentication(
                        server.Username,
                        server.Password,
                        false,
                        SqlLoginType.Standard);
                }
                Response.Redirect("databases.aspx");
            }
            else
            {
                ErrorLabel.Visible = true;
                ErrorLabel.Text = "无效的用户名和/或密码,您使用的是windows登录,不是你自己的,或服务器不存在.<br>";
            }
        }

        protected void AuthRadioButtonList_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            string authMethod = AuthRadioButtonList.SelectedItem.Value;

            switch (authMethod)
            {
                case "sql":
                    UsernameTextBox.Text = "";
                    UsernameTextBox.Enabled = true;
                    PasswordTextBox.Enabled = true;
                    break;
                case "windows":
                    if (Request.IsAuthenticated == true && security.WebServer == "iis")
                    {
                        UsernameTextBox.Text = User.Identity.Name;
                        lblCredMsg.Text = "请输入一个SQL服务器名称:";
                        UsernameTextBox.Enabled = false;
                        PasswordTextBox.Enabled = false;
                    }
                    break;
                default:
                    break;
            }
        }
    }
}