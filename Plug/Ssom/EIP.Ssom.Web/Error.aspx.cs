//=====================================================================
//
// THIS CODE AND INFORMATION IS PROVIDED TO YOU FOR YOUR REFERENTIAL
// PURPOSES ONLY, AND IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE,
// AND MAY NOT BE REDISTRIBUTED IN ANY MANNER.
//
// Copyright (C) 2003  Microsoft Corporation.  All rights reserved.
//
//=====================================================================

using System;
using System.Web.UI;

namespace EIP.Ssom.Web
{
    /// <summary>
    /// If an error occurs anywhere in the application, this page will be displayed.
    /// </summary>
    public partial class Error : Page
    {
        private string ErrorLookup(int id)
        {
            /* To handle more errors:
             * When catching the error, redirect to "Error.aspx?errorPassCode="
             * followed by the number of your error code, which can be any number
             * that is not used already in the switch statement below.
             * Finally, just add the case for that number in the switch statement
             * below and write a user-friendly error message.
             */
            switch (id)
            {
                case 1000:
                    return "数据库不存在";
                case 1001:
                    return "存储过程不存在";
                case 1002:
                    return "表不存在";
                case 1003:
                    return "列不存在";
                case 2001:  // DatabaseProperties.aspx (user might not have permission to view properties)
                    return "有一个问题阅读的属性数据库. " +
                        "有可能你未被授权查看或更改属性的数据库.";
                case 2002:  // Used whenever the application tries to connect to a database.
                    return "有一个问题连接到数据库。请检查, " +
                        "数据库可用,你的登录信息是正确的,你有权限来访问数据库.";
                default:
                    return "一个未知的错误发生。如果继续,请尝试退出并重新登陆.";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // There are two kinds of errors - custom errors with numbers, and uncaught exceptions
            if (Request["error"] != null)
            {
                ErrorLabel.Text = String.Format("Error {0}: {1}", Server.HtmlEncode(Request["error"]), ErrorLookup(Convert.ToInt32(Request["error"])));
            }
            else if (Request["errormsg"] != null || Request["stacktrace"] != null)
            {
                ErrorLabel.Text = "错误消息: <br>" + Request["errormsg"].Replace("\n", "<br>") + "<br><br>" +
                                  "堆栈信息: <br>" + Request["stacktrace"].Replace("\n", "<br>");
            }
            //else if (HttpContext.Current.Request.QueryString["errorPassCode"] != null)
            //// Check to see if there is an error code in the query string of the redirect url
            //{
            //    ErrorLabel.Text = ErrorLookup(Int32.Parse(HttpContext.Current.Request.QueryString["errorPassCode"]));

            //    Exception x = (Exception)Application["Error"];

            //    while (x != null)
            //    {
            //        ErrorLabel.Text += x.Message.Replace("\n", "<br>") + "<br><br>" + "<br><hr><br>";
            //        x = x.InnerException;
            //    }

            //    Application.Remove("Error");
            //}
            else
            {
                ErrorLabel.Text = "一个未知的错误发生。请再试一次.";

                Exception x = (Exception)Application["Error"];

                while (x != null)
                {
                    ErrorLabel.Text += x.Message.Replace("\n", "<br>") + "<br><br>" + x.StackTrace.Replace("\n", "<br>") + "<br><hr><br>";
                    x = x.InnerException;
                }

                Application.Remove("Error");
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion
    }
}
